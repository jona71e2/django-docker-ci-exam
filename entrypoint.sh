#!/bin/sh

echo "** RTE mode: ${RTE} - Running entrypoint"

case "$RTE" in
    dev )
        echo "** Development mode."
	python manage.py check
	python manage.py makemigrations --merge
	python manage.py migrate --noinput
        pip-audit
        # coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        # coverage report -m
        python manage.py runserver 0.0.0.0:8000
        ;;
    test )
        echo "** Test mode."
        # pip-audit || exit 1
	python manage.py check
	python manage.py makemigrations
	python manage.py migrate
        pip install coverage
        coverage run --source="." --omit=manage.py manage.py test --verbosity 2
        coverage report -m --fail-under=5
        ;;
    prod )
        echo "** Production mode."
        # pip-audit || exit 1
        python manage.py check --deploy
	python manage.py collectstatic --noinput
	gunicorn todo_project.asgi:application -b 0.0.0.0:8080 -k uvicorn.workers.UvicornWorker
        ;;
esac
