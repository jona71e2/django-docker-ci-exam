from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from .models import Todo


class UrlTest(TestCase):
    # see if you can load homepage
    def test_root(self):
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)


class ViewTests(TestCase):
    def setUp(self) -> None:
        # Create User
        self.client = Client()
        self.user_one = User.objects.create_user(
            username='Pip', email='pip@install.com', first_name='testuser', password='PIP!12345678')
        self.authenticated_user = self.client.login(username=self.user_one.username,
                                                    password='PIP!12345678')

    def test_Index_GET(self):
        response = self.client.get(reverse('todo_app:index'), follow=True)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'todo_app/index.html')

    def test_Index_POST(self):
        response = self.client.post(reverse('todo_app:index'), {
            'user': self.user_one,
            'text': 'Post Request Todo'
        }, follow=True)

        saved_todo = Todo.objects.get(text='Post Request Todo')
        todo_text = saved_todo.text
        todo_user = self.user_one.username

        self.assertEqual(response.status_code, 200)
        self.assertEqual(todo_text, 'Post Request Todo')
        self.assertEqual(todo_user, 'Pip')

    def test_completed_todos(self):
        response = self.client.get(reverse('todo_app:completed_todos'), user=self.user_one, follow=True)
        self.assertTemplateUsed(response, 'todo_app/completed_todos.html')


class TodoTestCase(TestCase):

    def setUp(self) -> None:
        # Create Users
        user_one = User.objects.create_user(
            'Pip', 'pip@install.com', 'testuser')
        user_two = User.objects.create_user(
            'Django', 'django@requirements.com', 'python3-mvenv')

        # Creating todos
        todo_one = Todo.objects.create(
            user=user_one, text='first', status=False)
        todo_two = Todo.objects.create(
            user=user_one, text='second', status=True)
        Todo.objects.create(user=user_two, text='third')
        Todo.objects.create(user=user_two, text='fourth', status=False)

        # first todo
        self.assertEqual(todo_one.text, 'first')
        self.assertEqual(todo_one.user.username, 'Pip')
        self.assertEqual(todo_one.status, False)

        # second todo
        self.assertEqual(todo_two.text, 'second')
        self.assertEqual(todo_two.user.username, 'Pip')
        self.assertEqual(todo_two.status, True)

    def test_change_status(self):
        first = Todo.objects.get(text='first')
        second = Todo.objects.get(text='second')
        first.status = True
        first.save()
        second.status = False
        second.save()
        assert first.status
        assert not second.status

    def test_delete_todo(self):
        # get user
        user_one = User.objects.get(email='pip@install.com')
        # create new todos
        first = Todo.objects.create(user=user_one, text='to be deleted')
        second = Todo.objects.create(
            user=user_one, text='This one will be deleted', status=False)

        # check length before delete
        todos_length = Todo.objects.all().count()
        self.assertEqual(6, todos_length)

        # delete todo
        first.delete()

        # check length and assert after delition
        todos_length = Todo.objects.all().count()
        self.assertEqual(5, todos_length)

        # delete todo
        second.delete()
        # check length and assert after delition
        todos_length = Todo.objects.all().count()
        self.assertEqual(4, todos_length)
        # end


class TestTodoAppModels(TestCase):
    def test_model_str(self):
        user = User.objects.create_user('Pip', 'pip@install.com', 'testuser')

        todo = Todo.objects.create(user=user, text='django')
        text = todo.text
        pk = todo.pk
        status = todo.status
        self.assertEqual(
            str(f"{user} - {pk} - {text} - {status}"), 'Pip - 1 - django - False')
