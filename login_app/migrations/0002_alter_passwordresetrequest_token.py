# Generated by Django 4.0.2 on 2022-02-25 09:55

from django.db import migrations, models
import secrets


class Migration(migrations.Migration):

    dependencies = [
        ('login_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='passwordresetrequest',
            name='token',
            field=models.CharField(db_index=True, default=secrets.token_urlsafe, max_length=43, unique=True),
        ),
    ]
