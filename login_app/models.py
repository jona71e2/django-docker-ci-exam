from django.db import models
from django.contrib.auth.models import User
from secrets import token_urlsafe


class PasswordResetRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=43, default=token_urlsafe, db_index=True, unique=True)
    created_timestamp = models.DateTimeField(auto_now_add=True)  # is never changed
    updated_timestamp = models.DateTimeField(auto_now=True)  # should be updated

    def __str__(self):
        return f'{self.user} - {self.created_timestamp} - {self.updated_timestamp} - {self.token}'
